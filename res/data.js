/**
 * Created by db on 15-5-13.
 */

var album = {
    id: "相册id",
    name: "相册名字",
    desc: "相册描述",
    pages: [{//相册页
        id: "页码id",
        order: "页码",
        frames: [{//相册页的帧
            id: "帧id",
            type: "帧类型：文字或者图片,这里是文字",
            order: "帧的顺序",
            anim_type: "动画类型",
            anim_start: "动画开始时间",
            anim_duration: "动画持续时间"
        }]
    }]
}